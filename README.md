# crewapp
<h3>This tool is a simple crm (customer relationship management) app. It is currently being built with a landscape 
company in mind, but can be applied to different generes with a similar or like business model.<h3/>

<h1>General Scope</h1>
<p>The app should have a navigation bar that can guide the user to selectable screens.
To begin, the navbar is going to have the following:</p>
<ul><li>Dashboard - This screen will house several windows displaying use full data.</li></ul>
<ul><li>Calendar - This screen is a calendar - The calendar will have assigned client property respective to the date.
The calendar should enable the user the following selectable views - month, week, day.</li></ul> 
<ul><li>Clients - This screen will contain an dictionary of the company's clients.</li></ul>
